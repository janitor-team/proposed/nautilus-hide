nautilus-hide (0.2.3-8.1) unstable; urgency=medium

  * Non-maintainer upload

  [ Nathan Pratta Teodosio ]
  * Patch for compatibility with nautilus 43 (Closes: #1018090)

  [ Jeremy Bicha ]
  * Add patch to fix gettext import with newer Python3 versions

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 01 Sep 2022 08:21:02 -0400

nautilus-hide (0.2.3-8) unstable; urgency=medium

  * Fix CMake script to allow out-of-place builds of translation
    files, so that backing up and restoring modified files during
    build can be dropped.
  * Simplify the declaration of the required Debhelper compat level
    by using the debian/control file only.
  * Add support for terse build option.
  * Switch to python3-nautilus.
    Thanks to Andreas Henriksson for report. (Closes: #942774)
  * Add Spanish translation.
  * Indicate compliance with Debian Policy 4.4.1 (no changes required).
  * Remove dependency on xdotool, so that Nautilus window can be refreshed
    in GNOME Wayland. Thanks to Andreas Henriksson for report.

 -- Carlos Maddela <e7appew@gmail.com>  Tue, 05 Nov 2019 07:14:19 +1100

nautilus-hide (0.2.3-7) unstable; urgency=medium

  * Build with Debhelper compat level 12.
  * Add more details to debian/upstream/metadata.
  * Update debian/copyright to use the same license ID as that
    used in the appstream metadata: GPL-3+ -> GPL-3.0+.
  * Update debian/gbp.conf to conform with DEP14 conventions.

 -- Carlos Maddela <e7appew@gmail.com>  Tue, 29 Jan 2019 12:16:40 +1100

nautilus-hide (0.2.3-6) unstable; urgency=low

  * Cherry-pick unreleased upstream fix for "RuntimeError: object
    at 0x... of type NautilusHide is not initialized".
  * Simplify process by which mutable files are backed up and restored.
  * Indicate compliance with Debian Policy 4.3.0 (no changes required).

 -- Carlos Maddela <e7appew@gmail.com>  Sat, 29 Dec 2018 06:02:47 +1100

nautilus-hide (0.2.3-5) unstable; urgency=low

  * Set "Rules-Requires-Root: no".
  * Revert to packaging upstream NEWS file as /usr/share/doc/package/NEWS.gz.
  * Allow build rules to be as verbose as possible.
  * Indicate compliance with Debian Policy 4.2.1.

 -- Carlos Maddela <e7appew@gmail.com>  Sat, 20 Oct 2018 19:04:54 +1100

nautilus-hide (0.2.3-4) unstable; urgency=low

  * Update Vcs-* details to official Debian repo.

 -- Carlos Maddela <e7appew@gmail.com>  Fri, 22 Jun 2018 06:31:19 +1000

nautilus-hide (0.2.3-3) unstable; urgency=low

  * Add machine-readable upstream metadata.
  * Indicate compliance with Debian Policy 4.1.4.

 -- Carlos Maddela <e7appew@gmail.com>  Fri, 27 Apr 2018 00:47:13 +1000

nautilus-hide (0.2.3-2) unstable; urgency=low

  * Relocate VCS to salsa.debian.org.
  * Build with Debhelper compatibility level 11.
  * Indicate compliance with Debian Policy 4.1.3.
  * debian/gbp.conf: formatting changes.
  * Update debian/watch to allow all available versions of upstream
    tarballs to be downloaded, not just the most recent ones.

 -- Carlos Maddela <e7appew@gmail.com>  Mon, 08 Jan 2018 07:03:15 +1100

nautilus-hide (0.2.3-1) unstable; urgency=low

  * New upstream release [0.2.3].
  * Make git-buildpackage sign tags.
  * Drop 01-build-german-translations.patch, already applied upstream.
  * Bump Standards Version to 4.1.2 (no changes required).
  * Backup and restore translation files, instead of relying on
    extend-diff-ignore source option.
  * Fix and update details in debian/copyright.
  * Add check to ensure copyright has an entry for each translation file.

 -- Carlos Maddela <e7appew@gmail.com>  Mon, 25 Dec 2017 05:30:00 +1100

nautilus-hide (0.2.1-1) unstable; urgency=low

  [ Bruno Nova ]
  * New upstream release [0.2.1].
  * debian/control:
    - Added xdotool to Depends and Build-Depends.
  * debian/copyright:
    - added license of po/de.po.
    - updated copyright of extension/nautilus-hide.py.
  * debian/source/options: added po/de.po to extend-diff-ignore regex.
  * Remove debian/install, debian/nautilus-admin-restart, debian/postinst
    and debian/prerm, removing the notification to restart nautilus.

  [ Carlos Maddela ]
  * New maintainer (Closes: #862211).
  * Add debian/gbp.conf.
  * Update VCS details.
  * Bump Standards Version to 4.0.1 and compat level to 10.
  * debian/control:
    - Perform wrap-and-sort.
    - Reformat package's description.
  * Package NEWS file as upstream changelog.
  * Update debian/copyright.
  * Include German translations.

 -- Carlos Maddela <e7appew@gmail.com>  Sun, 20 Aug 2017 08:57:57 +1000

nautilus-hide (0.1.3-1) unstable; urgency=low

  * New upstream release.
  * debian/control: updated Homepage and Vcs-* fields.
  * debian/copyright: updated Source field.
  * debian/docs: added to install AUTHORS, NEWS and README.md.
  * debian/watch: updated the URL.

 -- Bruno Nova <brunomb.nova@gmail.com>  Tue, 22 Sep 2015 20:21:27 +0100

nautilus-hide (0.1.2-1) unstable; urgency=low

  * Initial release. (Closes: #786348)

 -- Bruno Nova <brunomb.nova@gmail.com>  Fri, 22 May 2015 12:31:02 +0100
